package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IProjectToTaskService;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public class ProjectToTaskService implements IProjectToTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectToTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task taskBindById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task taskUnbindById(String projectId, String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.taskUnbindById(taskId);
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeById(String projectId) {
        removeAllTaskByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }
}
